---
title: 🏀 Balle rebondissante
author: N. Revéret
---

# 🏀 Balle rebondissante

Nous allons dans cette activité découvrir l'environnement de travail de NSI et, si nous avançons bien, jouer avec une balle rebondissante !

## L'environnement de travail

![Guido von Rossum](images/guido.jpg){ width=20% align=right }

Le langage de programmation utilisé principalement en NSI est le [`Python`](https://www.python.org/). Ce langage a été créé en 1992 par Guido von Rossum, un programmeur néerlandais qui travaille aujourd'hui pour Microsoft.

`Python` est un langage de programmation, il permet d'écrire des programmes qui sont exécutés par l'ordinateur.
N'importe quel logiciel de traitement de texte permet d'écrire du code `Python`, y compris *Word* !
Il est néanmoins plus facile d'utiliser un logiciel dédié, un *IDE* (pour *Integrated Development Environment*).
Il en existe de très nombreux, on utilise ici [**Thonny**](https://thonny.org/) qui est conçu pour les débutants en programmation.

??? question-nico "Démarrage"

    Lancer **Thonny**.
    
    Appeler le professeur si vous ne trouvez pas le logiciel : il n'est peut-être pas installé...


On trouve trois zones dans le logiciel :

1. tout en haut, le menu ;

2. la zone centrale est la zone de saisie du code. On y écrit les programmes ;

3. la console est située en bas. On peut y saisir des instructions élémentaires et les exécuter.

![Thonny](images/thonny-annote.png){width=50% .center}

??? question-nico "Bonjour le Monde !"

    Saisir `print("Hello World !")` dans la console. N'oubliez-pas d'appuyer sur "Entrée" !

Voilà, vous venez peut-être de faire vos premiers pas dans la programmation. C'est l'usage de commencer par un [`Hello World`](https://fr.wikipedia.org/wiki/Hello_world).

La console permet de faire tout ce que propose `Python`. On peut en particulier y créer des variables, les modifier, les afficher.

??? question-nico "Premières variables"

    Saisir les instructions suivantes dans la console (ne pas écrire les chevrons `>>>`, ils sont déjà là !) :

    ```pycon
    >>> a = 8
    >>> b = 10
    >>> c = a + b
    >>> print("La variable c vaut", c)
    ```

Il arrive **très** souvent que `Python` ne comprenne pas ce que nous voulons faire : il *bugge* !

Il est important de lire les messages d'erreur. Pour s'entraîner nous allons volontairement écrire des bêtises.

??? question-nico "Plantons `Python`"

    Saisir chacune des instructions *buggées* suivantes, lire et **comprendre** le message d'erreur :
    
    ```pycon
    >>> print(x)
    >>> 5 / 0
    >>> 12 + "texte"
    ```

Voilà pour la console, il est temps de passer aux vrais programmes !

## Premier programme

??? question-nico "Paramétrage de Thonny"

    Suivez les instructions du professeur pour vérifier le paramétrage de l'interpréteur Python.

??? question-nico "Le bon fichier"

    1. Télécharger le fichier [tp_1.py](python/tp_1.py){:download="tp_1.py"} ;
    2. L'ouvrir avec Thonny ;
    3. L'exécuter en cliquant sur ▶️.

Il est important de comprendre la structure générale d'un fichier `Python`. Lisez attentivement ce qui suit.

??? tip "La structure du programme"
    On trouve quatre zone dans ce programme :

    ```python
    #############
    # Import(s) # (1)
    #############
    from p5 import *

    ######################
    # Variables globales # (2)
    ######################
    BLANC = (255, 255, 255)
    NOIR = (0, 0, 0)
    ROUGE = (255, 0, 0)
    x = 100
    y = 100


    ####################################
    # Fonctions exécutées au lancement # (3)
    ####################################
    def setup():
        """Initialisation de la figure"""
        size(200, 200)


    def draw():
        """Dessin à chaque étape"""
        global x  # permet de modifier x (atuce)
        global y  # permet de modifier y (atuce)

        # Le fond
        background(*BLANC)

        # Dessin d'un disque
        fill(*NOIR)  # couleur intérieure
        stroke(*ROUGE)  # couleur du bord
        strokeWeight(2)  # largeur de la bordure
        circle(x, y, 50)


    def mouse_pressed():
        """Actions effectuées lors d'un clic de souris"""
        pass


    ##########################
    # Lancement du programme # (4)
    ##########################
    run(frame_rate=60)
    ```

    1.    Les imports permettent d'ajouter des fonctionnalités à `Python`.
        
        Ici on importe le module `p5` qui permet de faire du dessin.

    2.    La déclaration des variables globales.

        Ici on déclare deux couleurs ainsi que les coordonnées du disque

    3.    Les fonctions utilisées dans le programme.

        Dans le cas présent, leur noms sont imposés par `p5`.
        
    4.    Le corps principal du programme (une seule ligne !).

??? question-nico "Changement de disque"

    ![Les coordonnées](images/coordonnees.png){ width=50% align=right}
    Nous allons dessiner modifier le disque (sa position et sa bordure) :

    * modifier la valeur de `x` et `y` (variables globales) afin de que le disque soit plus bas et plus à gauche.

    * ajouter une variable globale `GRIS = (200, 200, 200)` ;
    
    * modifier la couleur du bord du disque ;
    
    * augmenter la largeur de la bordure du disque.



??? question-nico "Nouvelle figure"

    Modifier la figure :
   
    * afin qu'elle fasse 500 pixels sur 500 ;
    
    * afin d'afficher deux disques avec des nouvelles couleurs, de taille différentes ;
    
    * afin d'ajouter d'autres formes en vous aidant du tableau ci-dessous[^1] ou de la documentation de [p5 (Shape)](https://p5js.org/reference/#group-Shape).

    [^1]: inspiré de [ce site](https://www.carnets.info/nsi_premiere/parcours_p5/).
    
    | Syntaxe                                 | Description                                                                                  |
    | :-------------------------------------- | :------------------------------------------------------------------------------------------- |
    | `#!py point(x, y)`                      | Point de coordonnées (x, y)                                                                  |
    | `#!py line(x1, y1, x2, y2)`             | Segment reliant les points de coordonnées (x1, y1) et (x2, y2)                               |
    | `#!py circle(x, y, diamètre)`           | Cercle dont le centre est en (x, y) et le diamètre est fourni                                |
    | `#!py square(x, y, côté)`               | Carré dont le sommet en haut à gauche est en (x, y) et ayant le côté fourni                  |
    | `#!py rect(x, y, largeur, hauteur)`     | Rectangle dont le sommet en haut à gauche est en (x, y) et dont les dimensions sont fournies |
    | `#!py triangle(x1, y1, x2, y2, x3, y3)` | Triangle dont les trois sommets sont en (x1, y1), (x2, y2), et (x3, y3)                      |


## La balle rebondissante

Nous allons désormais faire se déplacer la balle et la faire rebondir contre les parois de la fenêtre.

??? question-nico "Préparation"

    Créer une figure de 200 pixels de large et et 500 de haut. Créez des variables globales pour ces deux paramètres. N'oubliez-pas de modifier l'instruction créant la figure !
    
    Placer la balle en `#!py x = 100`, `#!py y = 25` et lui donner un diamètre de `#!py 50`. Là encore, travaillez avec des variables globales.
    
![Vitesse](images/vitesse.svg){align=right width=30% .autolight}

Comment faire tomber la balle ? Il suffit de lui appliquer une vitesse !

Il nous faut donc :

1. créer deux variables globales `vx` et `vy` et leur donner des valeurs bien pensées (pour que la balle tombe verticalement) ;

2. dans la fonction `draw`, **avant l'affichage**, modifier l'ordonnée de la balle en faisant `y = y + vy` ;

??? question-nico "La chute"

    Faites tomber la balle !
    
    Si vous trouvez qu'elle tombe trop ou pas assez vite, vous pouvez modifier sa vitesse ou le `frame_rate` à la dernière ligne.
    
Bien, nous avons une balle qui tombe... éternellement ! Nous allons la faire rebondir. La démarche est simple : si la balle touche le sol, elle repart dans l'autre sens. Exprimé en langage algorithmique cela devient :

```text title="📝 Langage naturel"
Si la hauteur de la balle est supérieur à la hauteur de la fenêtre :
    La vitesse verticale de la balle devient -1
```

En `Python`, cela devient :

```python
if y > hauteur: # (1)
    vy = -1  # (2)
```

1. N'oubliez-pas les « deux-points ».
2. Notez le décalage de la seconde ligne. Il est indispensable.

??? question-nico "Premier rebond"

    Faites rebondir la balle contre le sol.
    
??? question-nico "Améliorations"

    On propose ci-dessous plusieurs améliorations en guise de « défis » :
    
    * ajustez le test de hauteur afin que la balle ne sorte pas de la fenêtre avant de rebondir ;

    * faites rebondir la balle au plafond en écrivant un autre test ;

    * donnez une vitesse horizontale à la balle et faites la rebondir contre les murs !

    ![Balles rebondissantes](images/balles.gif){width=40% .center}