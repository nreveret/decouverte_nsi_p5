#############
# Import(s) #
#############
from p5 import *
from random import randrange

######################
# Variables globales #
######################

# La fenêtre
LARGEUR = 1000
HAUTEUR = 600
# Les couleurs
BLANC = (255, 255, 255)
NOIR = (0, 0, 0)
GRIS = (50, 50, 50)

####################################
# Fonctions exécutées au lancement #
####################################
def setup():
    size(LARGEUR, HAUTEUR)
    
    # Décommenter pour modifier la bordure des figures
    # stroke_weight(10)
    # stroke(*BLANC)
    noLoop()


def draw():
    # Le fond
    background(*BLANC)

    # Votre code

    save_frame("mon_fond_d_ecran.png")  # ne pas oubliez de modifier le nom du fichier !


run()
