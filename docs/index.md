---
author: N. Revéret
title: "Accueil"
---
# 🛫 Découverte de la spécialité NSI

On propose ici quelques travaux pratiques permettant à des élèves de 2nde de découvrir la spécialité NSI.

Commençons de ce pas avec en faisant [rebondir une balle](01/tp_1.md) !
