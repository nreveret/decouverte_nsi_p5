#############
# Import(s) #
#############
from p5 import *

######################
# Variables globales #
######################

# La fenêtre
LARGEUR = 305
HAUTEUR = 507
# Les couleurs
BLANC = (255, 255, 255)
NOIR = (0, 0, 0)
ROUGE = (255, 0, 0)
BLEU = (0, 0, 255)
GRIS = (50, 50, 50)
# La balle
x_balle = LARGEUR // 2
y_balle = HAUTEUR // 2
vx = random_uniform(5, 2)
vy = random_uniform(5, 2)
DIAMETRE = 10

# La raquette
LARGEUR_RAQUETTE = LARGEUR // 5
HAUTEUR_RAQUETTE = HAUTEUR // 30
VITESSE_RAQUETTE = 4
x_raquette = LARGEUR // 2 - LARGEUR_RAQUETTE // 2
y_raquette = HAUTEUR - 2 * DIAMETRE - HAUTEUR_RAQUETTE


####################################
# Fonctions exécutées au lancement #
####################################
def setup():
    """Initialisation de la figure"""
    size(LARGEUR, HAUTEUR)


def draw():
    """Dessin à chaque étape"""
    # Le fond
    background(*GRIS)

    fill(*BLANC)  # couleur intérieure
    stroke(*BLANC)  # couleur du bord
    strokeWeight(2)  # largeur de la bordure
    circle(x_balle, y_balle, DIAMETRE)

    fill(*NOIR)  # couleur intérieure
    stroke(*ROUGE)  # couleur du bord
    strokeWeight(2)  # largeur de la bordure
    rect(x_raquette, y_raquette, LARGEUR_RAQUETTE, HAUTEUR_RAQUETTE)

    deplace_balle()
    deplace_raquette()
    collision()


def deplace_balle():
    global x_balle  # permet de modifier x (atuce)
    global y_balle  # permet de modifier y (atuce)
    global vx  # permet de modifier vx (atuce)
    global vy  # permet de modifier vy (atuce)

    # La balle
    x_balle += vx
    y_balle += vy
    if x_balle - DIAMETRE // 2 < 0:
        vx *= -1
    if x_balle + DIAMETRE // 2 > LARGEUR:
        vx *= -1
    if y_balle - DIAMETRE // 2 < 0:
        vy *= -1
    if y_balle + DIAMETRE // 2 > HAUTEUR:
        vy *= -1



def deplace_raquette():
    global x_raquette


    if key == "LEFT":
        if 0 <= x_raquette - VITESSE_RAQUETTE:
            x_raquette -= VITESSE_RAQUETTE
    if key == "RIGHT":
        if x_raquette + LARGEUR_RAQUETTE + VITESSE_RAQUETTE <= LARGEUR:
            x_raquette += VITESSE_RAQUETTE


def collision():
    global vy
    if (
        x_raquette <= x_balle <= x_raquette + LARGEUR_RAQUETTE
        and y_raquette <= y_balle + DIAMETRE // 2 <= y_raquette + HAUTEUR_RAQUETTE
    ):
        vy *= -1


##########################
# Lancement du programme #
##########################
run(frame_rate=60)
