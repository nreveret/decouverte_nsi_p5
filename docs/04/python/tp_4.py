#############
# Import(s) #
#############
from p5 import *

######################
# Variables globales #
######################

# La fenêtre
LARGEUR = 300
HAUTEUR = 500

# Les couleurs
BLANC = (255, 255, 255)
NOIR = (0, 0, 0)
ROUGE = (255, 0, 0)
BLEU = (0, 0, 255)
GRIS = (50, 50, 50)

# La balle
x_balle = ...
y_balle = ...
vx = random_uniform(2, 1)  # vitesse hoizontale aléatoire
vy = random_uniform(2, 1)  # vitesse verticale aléatoire
DIAMETRE = 10

# La raquette
LARGEUR_RAQUETTE = ...
HAUTEUR_RAQUETTE = ...
VITESSE_RAQUETTE = 4
x_raquette = ...
y_raquette = ...


####################################
# Fonctions exécutées au lancement #
####################################
def setup():
    """Initialisation de la figure"""
    size(LARGEUR, HAUTEUR)


def draw():
    """Dessin à chaque étape"""
    # Le fond
    background(*GRIS)

    deplace_balle()
    deplace_raquette()
    collision()


    # Dessin de la balle    
    fill(*BLANC)  # couleur intérieure
    stroke(*BLANC)  # couleur du bord
    strokeWeight(2)  # largeur de la bordure
    ...  # le dessin de la balle

    # Dessin de la raquette
    fill(*NOIR)  # couleur intérieure
    stroke(*ROUGE)  # couleur du bord
    strokeWeight(2)  # largeur de la bordure
    ...  # le dessin de la raquette


def deplace_balle():
    global x_balle  # permet de modifier x (atuce)
    global y_balle  # permet de modifier y (atuce)
    global vx  # permet de modifier vx (atuce)
    global vy  # permet de modifier vy (atuce)

    ...
    


def deplace_raquette():
    global x_raquette
    
    ...


def collision():
    global vy
    
    ...


##########################
# Lancement du programme #
##########################
run(frame_rate=60)
