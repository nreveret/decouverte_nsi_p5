---
title: 🤖 Du caillou à la puce
author: N. Revéret
---

# 🤖 Du caillou à la puce

L'*algorithme* est au cœur de la science informatique. [Wikipedia](https://fr.wikipedia.org/wiki/Algorithme) en donne la définition suivante :

!!! note "Définition"

    Un algorithme est une suite finie et non ambiguë d'instructions et d'opérations permettant de résoudre une classe de problèmes. 

Le but de ce TP est de découvrir quelques notions d'algorithmique, du langage naturel jusqu'à Python !

## Le langage naturel

Un algorithme peut être représenté sous différentes formes. Ci-dessous on représentant le même test sous forme d'un *algorigramme* et sous forme de *pseudo-code*.

![représentation](images/structure.svg){width=40% .center .autolight}

A notre petit niveau, il existe peu d'instructions dans les algorithmes :

| Instruction                                    | Rôle                                                                 |
| :--------------------------------------------- | :------------------------------------------------------------------- |
| `a` prend la valeur 2                          | Donne la valeur 2 à `a`                                              |
| Affiche `a`                                    | Affiche la valeur de `a`                                             |
| Lire un <type> et le stocker dans`a`           | Demande à l'utilisateur de saisir un <type> et l'enregistre dans `a` |
| Si <condition> alors <action1> sinon <action2> | Teste une condition et agit en conséquence                           |
| Pour `a` prenant les valeurs dans <ensemble>:  | Répète une action en donnant une valeur à `a`                        |
| Tant que <condition> répète <action>           | Répète une action tant que la condition est respectée                |


??? question-nico "Énigmes"

    Mettons cela en oeuvre en résolvant [ces énigmes !](https://nreveret.forge.apps.education.fr/enigmes/naturel/1/)

## Puzzle de Parsons

Bien. Nous avons découvert comment lire des instructions en langage naturel. Il nous faut maintenant rédiger des algorithmes.

Cette étape est compliquée pour différentes raisons. Supposons que nous ayons un problème à résoudre. Nous devons :

1. comprendre le problème
2. résoudre ce problème !
3. expliquer clairement la démarche à suivre
4. saisir cette démarche en langage informatique

Les **puzzles de Parsons** permettent de se familiariser avec ces étapes : le code est déjà saisi, il faut juste l'organiser.

??? question-nico "Puzzle 1"

    Faisons un premier exemple ([lien direct](https://www.codepuzzle.io/IPP5BHG)):
    
    <iframe src="https://www.codepuzzle.io/IPP5BHG" width="100%" height="600" frameborder="0"></iframe>

??? question-nico "Puzzle 2"

    Bien c'était très simple. Compliquons un peu les choses ([lien direct](https://www.codepuzzle.io/IPJ29QK)):
    
    ??? tip "Astuce"
    
        La fonction `#!py input` pose une question à l'utilisateur.
        
        La fonction `#!py int` convertit un texte en un nombre entier (`integer` en anglais).
    
    <iframe src="https://www.codepuzzle.io/IPJ29QK" width="100%" height="600" frameborder="0"></iframe>

??? question-nico "Puzzle 3"

    Savez-vous calculer une moyenne ? ([lien direct](https://www.codepuzzle.io/IPY6BRG))
    
    !!! warning "Faites les choses dans l'ordre"
    
        Déplacez les lignes à la bonne position **avant** d'ouvrir les menus déroulants.
    
    <iframe src="https://www.codepuzzle.io/IPY6BRG" width="100%" height="400" frameborder="0"></iframe>

??? question-nico "Puzzle 4"

    Chantons ensemble ! ([lien direct](https://www.codepuzzle.io/IPVQBEF))
    
    
    <iframe src="https://www.codepuzzle.io/IPVQBEF" width="100%" height="600" frameborder="0"></iframe>

??? question-nico "Puzzle 5"

    Connaissez-vous vos voyelles ? ([lien direct](https://www.codepuzzle.io/IPCBJHW))
    
    <iframe src="https://www.codepuzzle.io/IPCBJHW" width="100%" height="600" frameborder="0"></iframe>
    

## Tout seul !

Il est temps d'écrire du code par soi-même !

??? question-nico "Fonctions simples"

    [Écrire des fonctions simples](https://codex.forge.apps.education.fr/exerices/fonctions/sujet/){.md-button target="_blank"}

??? question-nico "Consommation électrique"

    [NSI $\times$ PC](https://codex.forge.apps.education.fr/exerices/consommation_electrique/sujet/){.md-button target="_blank"}

??? question-nico "Domaine des entiers"

    [Jusqu'à combien sait compter l'ordinateur ?](https://codex.forge.apps.education.fr/exerices/plage_entiers/sujet/){.md-button target="_blank"}

??? question-nico "Nombre de bits"

    [Taille d'un nombre en binaire](https://codex.forge.apps.education.fr/exerices/nb_bits/sujet/){.md-button target="_blank"}