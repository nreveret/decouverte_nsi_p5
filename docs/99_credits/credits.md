---
author: N. Revéret
title: 👍🏼 Crédits
---
# 👍🏼 Crédits

Site créé par N. Revéret `nreveret<à>ac<tiret>rennes<point>fr`.

Le logo provient de <a href="https://www.flaticon.com/free-icons/encounter" title="encounter icons">Encounter icons created by Freepik - Flaticon</a>.

L'algorigramme utilisée dans le TP 3 provient de [ce site](https://j-serrand.github.io/sti2d-docs/pages/1ere/Sequence_algorithmique/00-Cours_algo/ressources/structure_si.svg).